#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

yum install -y anaconda anaconda-runtime createrepo isomd5sum genisoimage rpmdevtools

mkdir -p /opt/iso/build
cd /opt/iso/build

mkdir -p ~/kickstart_build/isolinux/{images,ks,LiveOS,Packages,postinstall}


