exec 1>/tmp/log 2>&1
mounted_path="/opt/iso/mounted"
#mkdir -p /mnt/iso
#mount -o loop /opt/iso/CentOS-7-x86_64-Minimal-1604-01.iso /mnt/iso



cp $mounted_path/.discinfo ~/kickstart_build/isolinux/
cp $mounted_path/isolinux/* ~/kickstart_build/isolinux/
rsync -av $mounted_path/images/ ~/kickstart_build/isolinux/images/
cp $mounted_path/LiveOS/* ~/kickstart_build/isolinux/LiveOS/


cp $mounted_path/repodata/*comps.xml.gz ~/kickstart_build/

cd ~/kickstart_build/
gunzip *comps.xml

mv *comps.xml comps.xml


## Packages 

mkdir /tmp/packages
cd /tmp/packages

while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "Text read from file: $line"
done < /opt/iso/package_list

rm -Rf index* repodata

rsync -av $mounted_path/Packages/ ~/kickstart_build/isolinux/Packages/
rsync -av /tmp/packages/ ~/kickstart_build/isolinux/Packages/


## Package Dependenciees check 
cd ~/kickstart_build/isolinux
createrepo -g ~/kickstart_build/comps.xml .


## KickStart 

cp /opt/iso/base_ks.cfg ks.cfg


# Generate ISO 

cd ~/kickstart_build/
mkisofs -o vidyalaya.iso -b isolinux.bin -c boot.cat -no-emul-boot -V 'Vidyalaya 1.0 x86_64' -boot-load-size 4 -boot-info-table -R -J -v -T isolinux/
