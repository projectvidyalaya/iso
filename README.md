Please see updated contributions in develop branch

How to use this repo:

Note : This repo is a work in progress. So expect things to break, instructions not clean and all sorts of dirty hacks inside.

1. You would need docker installed. Visit the docker website for install instructions

2. Once done, and you are sure docker has started, download the latest centos docker image

3. Also have the latest CentOS iso downloaded in the repo clone path. 
Once you have clone , also mount the iso in a folder called mounted.

cd /path/to/cloned/iso/repo
wget http://buildlogs.centos.org/rolling/7/isos/x86_64/CentOS-7-x86_64-Minimal-1604-01.iso

mount -o loop,ro CentOS-7-x86_64-Minimal-1604-01.iso mounted

4. I have cloned this repo in the /iso directory. You can clone it anywhere, but change the path in the below command to replect the correct path. Please use absolute paths and not relative paths.

sudo docker run -itv /iso/:/opt/iso centos:latest bash

5. Now that you are inside the docker, cd /opt/iso
You should find 2 scripts called setup.sh and create_iso.sh

First run setup.sh

Once done, run create_iso.sh.

This would give you an iso called vidyalaya.iso in your ~/kickstart_build dir.

Voila ! There you go.


Please note that, this is still very initial phase code, and works for now possibly only for me. Will have a cleaner iso generation script soon.

If you interested in contributing to this code, please join the mailing list @ http://freelists.org/list/projectvidyalaya

